/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'demo\'">' + entity + '</span>' + html;
	}
	var icons = {
			'demo-icon-document-stroke' : '&#xe000;',
			'demo-icon-star' : '&#xf005;',
			'demo-icon-star-empty' : '&#xf006;',
			'demo-icon-user' : '&#xf007;',
			'demo-icon-ok' : '&#xf00c;',
			'demo-icon-remove' : '&#xf00d;',
			'demo-icon-trash' : '&#xf014;',
			'demo-icon-tag' : '&#xf02b;',
			'demo-icon-tags' : '&#xf02c;',
			'demo-icon-book' : '&#xf02d;',
			'demo-icon-flag' : '&#xf024;',
			'demo-icon-search' : '&#xf002;',
			'demo-icon-edit' : '&#xf044;',
			'demo-icon-check' : '&#xf046;',
			'demo-icon-bullhorn' : '&#xf0a1;',
			'demo-icon-thumbs-up' : '&#xf087;',
			'demo-icon-calendar' : '&#xf073;',
			'demo-icon-group' : '&#xf0c0;',
			'demo-icon-comment' : '&#xf075;',
			'demo-icon-pushpin' : '&#xf08d;',
			'demo-icon-user-add' : '&#xe001;',
			'demo-icon-add-to-list' : '&#xe002;',
			'demo-icon-bell' : '&#xe003;',
			'demo-icon-check-empty' : '&#xf096;',
			'demo-icon-lock' : '&#xf023;',
			'demo-icon-comments-alt' : '&#xf0e6;',
			'demo-icon-comment-alt' : '&#xf0e5;',
			'demo-icon-quote' : '&#xe004;',
			'demo-icon-book-2' : '&#xe005;',
			'demo-icon-book-3' : '&#xe006;',
			'demo-icon-video' : '&#xe007;',
			'demo-icon-calendar-2' : '&#xe008;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/demo-icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};